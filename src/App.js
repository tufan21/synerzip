/* eslint-disable react/destructuring-assignment */

import React, {Component} from 'react';
import {SafeAreaView, StyleSheet} from 'react-native';
import InitScreen from './InitScreen';

class App extends Component {
  render() {
    return (
      <SafeAreaView style ={styles.safeArea} >
        <InitScreen />
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  safeArea: {
    flex: 1,
    backgroundColor: '#ddd',
  },
});
export default App;
