import React from 'react';
import {View, Alert} from 'react-native';
import {connect} from 'react-redux';
import {Icon} from 'react-native-elements';
import styles from './components/UserList/styles/styles';

//Importing all action creators
import * as actions from './store/actions/index';

//Custom Components
import UserListContainer from './components/UserList/UserContainer/UserListContainer';
import TitleComponent from './components/TitleComponent/TitleComponent';

class UserList extends React.Component {
  //Navigation option to create menu in header
  navigationOptions = ({navigation, navigationOptions}) => {
    return {
      headerTitle: 'User List',
    };
  };
  createUser = () => {
    this.props.navigation.navigate('CreateUser', {isUpdate: false});
  };
  //When component mounts api call is made
  componentDidMount = () => {
    this.props.onfetchUsers();
    this.props.navigation.setOptions({
      headerRight: () => (
        <View style={{flexDirection: 'row'}}>
          <View style={{paddingRight: 10}}>
            <Icon
              name="create"
              type="materialIcons"
              color="#000"
              onPress={this.createUser}
            />
          </View>
        </View>
      ),
    });
  };

  //Function used to alert string
  //Used to alert name of selected user
  navigateToUserDetails = (userID) => {
    this.props.navigation.navigate('UserDetails', {id: userID});
  };

  //function that gets called when user scrolls to end of list
  endOfListReached = () => {
    this.props.onfetchUsers();
  };

  render() {
    // console.log('this.props.userlist',this.props.userlist)
    return (
      <View style={styles.container}>
        <UserListContainer
          users={this.props.userlist}
          onItemSelected={this.navigateToUserDetails}
          endReached={this.endOfListReached}
        />
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    userlist: state.users.users,
    selectedUsers: state.users.selectedUsers,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    onfetchUsers: () => dispatch(actions.fetchUsers()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(UserList);
