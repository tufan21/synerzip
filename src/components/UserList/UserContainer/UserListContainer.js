import React, {Component} from 'react';
import {FlatList, View} from 'react-native';
import styles from './../styles/styles';

import UserListItem from '../ListItem/UserListItem';

class ListContainer extends Component {
  render() {
    return (
      <View style={styles.listHeight}>
        <FlatList
          style={styles.listContainer}
          data={this.props.users}
          renderItem={(info) => (
            <UserListItem
              key={info.item.id}
              userId={info.item.id}
              userEmail={info.item.email}
              userName={info.item.name}
              onItemPressed={() => {
                this.props.onItemSelected(info.item.id);
              }}
            />
          )}
          keyExtractor={(item) => item.id.toString()}
          onEndReached={() => {
            if (!this.onEndReachedCalledDuringMomentum) {
              if (this.props.users.length !== 0) {
                this.props.endReached();
              }
            }
          }}
          onEndReachedThreshold={0.1}
          bounces={false}
          onMomentumScrollBegin={() => {
            this.onEndReachedCalledDuringMomentum = false;
          }}
        />
      </View>
    );
  }
}

export default ListContainer;
