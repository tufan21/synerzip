import React from 'react';
import {Text, View, TouchableOpacity} from 'react-native';
import styles from '../styles/styles';

const UserListItem = (props) => (
  <TouchableOpacity  onPress={props.onItemPressed}>
    <View style={styles.listItem}>
      <View style={styles.userDetails}>
        <Text style={styles.nameStyling}>Name: {props.userName}</Text>
        <Text style={styles.emailStyling}>Email: {props.userEmail}</Text>
      </View>
    </View>
  </TouchableOpacity>
);

export default UserListItem;
