import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  listItem: {
    flex: 1,
    width: '100%',
    marginBottom: 5,
    padding: 20,
    backgroundColor: '#eee',
    flexDirection: 'row',
    alignItems: 'center',
  },
  userDetails: {
    flex: 1,
    flexDirection: 'column',
    width: '100%',
  },
  placeButton: {
    padding: 10,
    borderRadius: 5,
  },
  emailStyling: {
    fontSize: 12,
  },

  nameStyling: {
    fontSize: 14,
    paddingBottom: 5,
  },
  CenterAlign: {
    paddingBottom: 5,
  },
  buttonStyling: {
    color: 'white',
  },
  listContainer: {
    width: '100%',
  },
  listHeight: {
    width: '100%',
  },
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  titleView: {
    padding: 20,
  },
  titleText: {
    textAlign: 'center',
    fontSize: 20,
  },
});

export default styles;
