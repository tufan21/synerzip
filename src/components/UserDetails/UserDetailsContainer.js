import React, {Component} from 'react';
import {Text, View, TouchableOpacity} from 'react-native';
import {Icon} from 'react-native-elements';

import {connect} from 'react-redux';
import styles from './styles';

//Importing all action creators
import * as actions from '../../store/actions/index';
class UserDetailsContainer extends Component {
  //Navigation option to create menu in header
  navigationOptions = ({navigation, navigationOptions}) => {
    return {
      headerTitle: 'User Details',
    };
  };
  deleteUser = () => {
    this.props.onDeleteUser(this.props.route.params.id, this.props.navigation);
  };
  updateUser = () => {
    this.props.navigation.navigate('CreateUser', {isUpdate: true});
  };

  //When component mounts api call is made
  componentDidMount = () => {
    this.props.navigation.setOptions({
      headerTitle: 'User Details',
      headerRight: () => (
        <View style={{flexDirection: 'row'}}>
          <View style={{paddingRight: 10}}>
            <Icon
              name="delete"
              type="antdesign"
              color="#000"
              onPress={this.deleteUser}
            />
          </View>
          <View style={{paddingRight: 10}}>
            <Icon
              name="edit"
              type="antdesign"
              color="#000"
              onPress={this.updateUser}
            />
          </View>
        </View>
      ),
    });

    this.props.onFetchUserDetails(this.props.route.params.id);
  };

  render() {
    const {userDetails} = this.props;
    return (
      <View>
        <View style={styles.listHeight}>
          <Text style={styles.nameStyling}>
            Name : {userDetails === null ? '' : userDetails.name}
          </Text>
          <Text style={styles.nameStyling}>
            Phone : {userDetails === null ? '' : userDetails.phone}
          </Text>
          <Text style={styles.nameStyling}>
            Email ID : {userDetails === null ? '' : userDetails.email}
          </Text>
          <Text style={styles.nameStyling}>
            WebSite : {userDetails === null ? '' : userDetails.website}
          </Text>
        </View>
        <View style={styles.listHeight}>
          <Text style={styles.nameStyling}>Address :</Text>
          <Text style={styles.nameStyling}>
            {userDetails === null
              ? ''
              : userDetails.address.suite +
                ',' +
                userDetails.address.street +
                ',' +
                userDetails.address.city +
                ',' +
                userDetails.address.zipcode}
          </Text>
        </View>
      </View>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    userDetails: state.users.userDetails,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    onFetchUserDetails: (id) => dispatch(actions.fetchUserDetails(id)),
    onDeleteUser: (id, navigation) =>
      dispatch(actions.deleteUser(id, navigation)),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(UserDetailsContainer);
