import React, {Component} from 'react';
import {Text, View, TextInput, TouchableOpacity} from 'react-native';

import {connect} from 'react-redux';
import styles from './styles';

//Importing all action creators
class CreateUser extends Component {
  //Navigation option to create menu in header
  navigationOptions = ({navigation, navigationOptions}) => {
    return {
      headerTitle: 'Create User',
    };
  };
  state = {
    name: '',
    email: '',
    phone: '',
    address: '',
    username: '',
  };
  handleEmail = (text) => {
    this.setState({email: text});
  };
  handleName = (text) => {
    this.setState({name: text});
  };
  handlePhone = (text) => {
    this.setState({phone: text});
  };
  handleAddress = (text) => {
    this.setState({address: text});
  };
  handleUserName = (text) => {
    this.setState({username: text});
  };
  submitData = () => {
    alert('not implemented');
  };
  //When component mounts api call is made
  componentDidMount = () => {
    this.props.navigation.setOptions({
      headerTitle: this.props.route.params.isUpdate
        ? 'Update User'
        : 'Create user',
    });
  };

  render() {
    return (
      <View>
        <TextInput
          style={styles.input}
          underlineColorAndroid="transparent"
          placeholder="Name"
          placeholderTextColor="#000"
          autoCapitalize="none"
          onChangeText={this.handleName}
        />
        <TextInput
          style={styles.input}
          underlineColorAndroid="transparent"
          placeholder="Email"
          placeholderTextColor="#000"
          autoCapitalize="none"
          onChangeText={this.handleEmail}
        />
        <TextInput
          style={styles.input}
          underlineColorAndroid="transparent"
          placeholder="Phone Number"
          placeholderTextColor="#000"
          autoCapitalize="none"
          onChangeText={this.handlePhone}
        />
        <TextInput
          style={styles.input}
          underlineColorAndroid="transparent"
          placeholder="UserName"
          placeholderTextColor="#000"
          autoCapitalize="none"
          onChangeText={this.handleUserName}
        />
        <TextInput
          style={styles.inputMultiline}
          underlineColorAndroid="transparent"
          placeholder="Address"
          placeholderTextColor="#000"
          multiline={true}
          autoCapitalize="none"
          onChangeText={this.handleAddress}
        />
        <TouchableOpacity
          style={styles.submitButton}
          onPress={() => this.submitData()}>
          <Text style={styles.submitButtonText}>
            {this.props.route.params.isUpdate ? 'Update' : 'Save'}
          </Text>
        </TouchableOpacity>
      </View>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    userDetails: state.users.userDetails,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(CreateUser);
