import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  listItem: {
    flex: 1,
    width: '100%',
    marginBottom: 5,
    padding: 20,
    backgroundColor: '#eee',
    flexDirection: 'row',
    alignItems: 'center',
  },
  input: {
    margin: 15,
    height: 40,
    borderColor: '#d3d3d3',
    borderWidth: 1,
    backgroundColor: '#FFF',
    paddingLeft: 10,
  },
  inputMultiline: {
    margin: 15,
    height: 100,
    borderColor: '#d3d3d3',
    borderWidth: 1,
    backgroundColor: '#FFF',
    paddingLeft: 10,
    textAlignVertical: 'top',
  },
  submitButton: {
    backgroundColor: 'red',
    padding: 10,
    margin: 15,
    height: 40,
  },
  submitButtonText: {
    color: 'white',
    justifyContent :"center",
    alignSelf: 'center'
  },
});

export default styles;
