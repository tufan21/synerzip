'use strict';
import React, {Component} from 'react';

import {View, StyleSheet, Button} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';

import {connect} from 'react-redux';

import HomeScreen from './UserList';
import UserDetails from './components/UserDetails/UserDetailsContainer';

import CreateUSer from './components/UserUpdate/CreateUser';

const Stack = createStackNavigator();

function MyStack() {
  return (
    <Stack.Navigator
      initialRouteName="Home"
      screenOptions={{gestureEnabled: false}}>
      <Stack.Screen
        name="Home"
        component={HomeScreen}
        options={HomeScreen.navigationOptions}
      />
      <Stack.Screen
        name="UserDetails"
        component={UserDetails}
        options={UserDetails.navigationOptions}
      />
      <Stack.Screen
        name="CreateUser"
        component={CreateUSer}
        options={CreateUSer.navigationOptions}
      />
    </Stack.Navigator>
  );
}

class InitScreen extends Component {
  render() {
    // console.log('response', this.props.isLoading)
    return (
      <NavigationContainer>
        <MyStack />
      </NavigationContainer>
    );
  }
}

InitScreen.propTypes = {};
const mapStateToProps = (state) => ({});
const mapDispatchToProps = (dispatch) => ({});

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(InitScreen);
