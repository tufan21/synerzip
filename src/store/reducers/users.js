import {
  JSON_RECEIVED,
  ERROR_GENERATED,
  JSON_RECEIVED_USER_DETAILS,
  ERROR_GENERATED_USER_DETAILS,
  DELETE_USER_SUCCESS,
  ERROR_GENERATED_USER_DELETE,
} from '../actions/types';
import {act} from 'react-test-renderer';

const initialState = {
  users: [],
  selectedPlace: null,
  usersImported: 0,
  error: null,
  userDetails: null,
  isUserDeleted: false,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case JSON_RECEIVED:
      let updatedUsers = state.users.slice();
      action.payload.data.map((user) => {
        updatedUsers.push(user);
      });
      return {
        ...state,
        users: updatedUsers,
        usersImported: state.usersImported + 10,
      };
    case ERROR_GENERATED:
      return {
        ...state,
        error: action.error,
      };

    case JSON_RECEIVED_USER_DETAILS:
      let selectedUser = action.payload.data;
      return {
        ...state,
        userDetails: selectedUser,
      };
    case ERROR_GENERATED_USER_DETAILS:
      return {
        ...state,
        error: action.error,
      };
    case DELETE_USER_SUCCESS:
      //let selectedUser = action.payload.data;
      return {
        ...state,
        isUserDeleted: true,
      };
    case ERROR_GENERATED_USER_DELETE:
      return {
        ...state,
        isUserDeleted: false,
      };
    default:
      return state;
  }
};

export default reducer;
