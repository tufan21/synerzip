import {JSON_RECEIVED} from '../actions/types';
import {JSON_RECEIVED_USER_DETAILS} from '../actions/types';

//Middleware used to intercept data from api and update them with a number
//to ensure no duplicates of ID or email get created
const updateInformation = (store) => (next) => (action) => {
  next(action);
};

export default updateInformation;
