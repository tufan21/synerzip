import axios from 'axios';

import {
  JSON_RECEIVED,
  ERROR_GENERATED,
  ERROR_GENERATED_USER_DETAILS,
  JSON_RECEIVED_USER_DETAILS,
  DELETE_USER_SUCCESS,
  ERROR_GENERATED_USER_DELETE,
} from './types';

//Action creator used to fetch data from server
export const fetchUsers = () => {
  return async (dispatch) => {
    function onSuccess(success) {
      dispatch({type: JSON_RECEIVED, payload: success});
      return success;
    }
    function onError(error) {
      dispatch({type: ERROR_GENERATED, error});
      return error;
    }
    try {
      const success = await axios.get(
        'https://jsonplaceholder.typicode.com/users',
      );
      return onSuccess(success);
    } catch (error) {
      return onError(error);
    }
  };
};

//Action creator used to fetch data from server
export const fetchUserDetails = (user_id) => {
  return async (dispatch) => {
    function onSuccess(success) {
      console.log('success',success)
      dispatch({type: JSON_RECEIVED_USER_DETAILS, payload: success});
      return success;
    }
    function onError(error) {
      dispatch({type: ERROR_GENERATED_USER_DETAILS, error});
      return error;
    }
    try {
      const success = await axios.get(
        `https://jsonplaceholder.typicode.com/users/${user_id}`,
      );
      return onSuccess(success);
    } catch (error) {
      return onError(error);
    }
  };
};

//Action creator used to fetch data from server
export const deleteUser = (user_id,navigation) => {
  return async (dispatch) => {
    function onSuccess(success) {
      console.log('success',success)
      dispatch({type: DELETE_USER_SUCCESS, payload: success});
      dispatch(fetchUsers())
      navigation.goBack()
      return success;
    }
    function onError(error) {
      dispatch({type: ERROR_GENERATED_USER_DELETE, error});
      return error;
    }
    try {
      const success = await axios.delete(
        `https://jsonplaceholder.typicode.com/users/${user_id}`,
      );
      return onSuccess(success);
    } catch (error) {
      return onError(error);
    }
  };
};
